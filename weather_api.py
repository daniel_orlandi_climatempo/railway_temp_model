import requests
from urllib.parse import urlencode

class WeatherApi:

    def __init__(self, authConfig, headers=''):
        self.data = urlencode(authConfig)
        self.headers = headers

        if headers == '':
            self.headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': str(self.data.__sizeof__())
            }


    def post(self, url):
        return requests.post(url, data=self.data, headers=self.headers)


    def get(self, url):
        return requests.get(url)


"""#example using WeatherApi class
data = {
    'key': '671',
    'client': 'consultoria',
    'nocache': True,
    'type': 'json',
    'application': 'site',
    'product': 4,
    'user':'psr',
    'password':'psr@monitor.1',
    'secret':'cvale'
}

api = WeatherApi(data, '')
req = api.post("http://api.climatempo.com.br/api/v1//forecast/period/radiation?localeId=3477&dateBegin=2015-05-12&dateEnd=2015-05-16")
#print request result in terminal
print(req.json())"""




