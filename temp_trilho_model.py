import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as scp
from dateutil import relativedelta as rd


class ForecastVariables(object):


    def __init__(self,
                 radiation_ = None,                 
                 wind_= None,
                 temperature_= None,
                 humidity_= None,
                 rain_= None,
                 dateBegin_= None,                 
                 lat_= None,
                 lon_= None):


        self.radiation_ = radiation_        


        self.wind_ = wind_


        self.temperature_ =temperature_


        self.humidity_ = humidity_


        self.rain_ = rain_


        self.dateBegin_ = dateBegin_                


class RailModel(object):


    def __init__(self):


    #ASCE 60 light rail
        self.model_ = np.asarray([[+30.1625, +1000,  +108],  
                                 [-30.1625, +1000,  +108],
                                 [-30.1625, +1000,   +84],
                                 [-6.15000, +1000,   +77],
                                 [-6.15000, +1000, +19.5],
                                 [-54.0000, +1000,    +7],
                                 [-54.0000, +1000,     0],
                                 [+54.0000, +1000,     0],
                                 [+54.0000, +1000,    +7],
                                 [+6.15000, +1000, +19.5],
                                 [+6.15000, +1000,   +77],
                                 [+30.1625, +1000,   +84],
                                 [+30.1625, -1000,  +108],
                                 [-30.1625, -1000,  +108],
                                 [-30.1625, -1000,   +84],
                                 [-6.15000, -1000,   +77],
                                 [-6.15000, -1000, +19.5],
                                 [-54.0000, -1000,    +7],
                                 [-54.0000, -1000,     0],
                                 [+54.0000, -1000,     0],
                                 [+54.0000, -1000,    +7],
                                 [+6.15000, -1000, +19.5],
                                 [+6.15000, -1000,   +77],
                                 [+30.1625, -1000,   +84]])
  

    #Reflectance in rail (%)
        self.rfl_ = 18.9 #10 yrs old rail, according to arcticle.
    

    #Emissivity coeff for rusted iron (aproximation)

        self.epsilon_ = .61

    #Considering the rail profile as a rectangle (simplification).
        self.Ar_ = 2*(2000*180) + (2000*60.3) #(mm²)

    #Rail volume in mm³
        self.Vr_ = 2000*(3897)


    #Rail Mass in kg/m
        self.Rm_ = 29.76


    #Rail specific heat (J/Kg K)
        self.c_r_ = .46


class RailTemp(object):
    
    
    def __init__(self, forecast_variables_ = None, rail_model_= None):
                 
        self.forecast_variables_ =  forecast_variables_  


        self.rail_model_ = rail_model_           
         

        self._base_day = '2000-01-01 00:00:00' 
        
             
    @staticmethod   
    def convert_to_timestamp(date_):
        
        
        if(type(date_) is not str):
            
            
                raise TypeError ('date_ must be of str type')           
        
        
        converted_date = pd.to_datetime(date_,
                                        yearfirst=True,
                                        utc=True,
                                        format='%Y-%m-%d %H:%M:%S')
        
        
        return converted_date
        

    @staticmethod    
    def compute_ecliptic_coordinates(date_):

                    
        julian_day = date_.to_julian_date()
        
        
        n = julian_day - 245145.0                  
        
                
        mean_lon = 280.46 + 0.9856474 * n #(0 <= L < 360°)
        
        
        mean_anomaly = 357.528 + 0.9856003 * n #(0 <= g < 360°)
        
        
        ecliptic_longitude = (mean_lon 
                              + 1.915 
                              * np.sin(mean_anomaly)
                              + 0.020 
                              * np.sin(2*mean_anomaly)) #(0 <= l < 360°)
        
                    
        ecliptic_obliquity = 23.439 -4e-6 * n #(degs)
        
        
        return(n,
               mean_anomaly,
               mean_lon,
               ecliptic_longitude,
               ecliptic_obliquity)
    
    
    @staticmethod
    def compute_celestial_and_local_coord(n_, g_, L_, l_, ep_, lat_,lon_,
                                                         full_return_ = False):
        
        
        
        
        right_ascension = np.arctan((np.cos(ep_)
                                    * np.sin(l_))
                                    / np.cos(l_))
        
        
        declination = np.arcsin(np.sin(ep_) * np.sin(l_))
        
        
        gmst = 6.69375 + 0.067098242 * n_  # (0 <= gmst < 24)
        
        
        lmst = gmst + ((lon_)/15) # (0 <= lmst < 24)
         
            
        ha = lmst-right_ascension
        
        
        elevation = np.arcsin(np.sin(declination)
                              * np.sin(lat_)
                              + np.cos(declination)
                              * np.cos(lat_))
        
        
        azimuth = np.arcsin((-(np.cos(declination))
                             * np.sin(ha))
                             / np.cos(elevation))
        

        if(full_return_ == True):
        
        
            return (elevation, azimuth, n_, g_, L_, l_, ep_)


        else:

            return (elevation, azimuth, L_)


    @staticmethod
    def compute_sun_vector(phi_, alpha_, L_):      
        
       
        sun_pos_X = (L_* np.cos(alpha_) * np.sin(phi_))


        sun_pos_Y = (L_* np.cos(alpha_) * np.cos(phi_))


        sun_pos_Z = (L_* np.sin(alpha_))

    
        return (sun_pos_X, sun_pos_Y, sun_pos_Z, phi_, alpha_)


    @staticmethod
    def compute_xy_projection(phi_, alpha_, rail_model_):       
        
        
        x_y_projection = []


        for each_row in rail_model_:
            

            x_n = each_row[0]


            y_n = each_row[1]


            z_n = each_row[2]


            x_line_n = - (((np.sin(phi_)/np.tan(alpha_))
                       * z_n)
                       + x_n)


            y_line_n = - (((np.cos(phi_)/np.tan(alpha_))
                       * z_n)
                       + y_n)


            x_y_projection.append(x_line_n, y_line_n)


        return np.asarray(x_y_projection)


    @staticmethod    
    def compute_h_conv(wind_):


        if (wind_ <= 5):


            h_conv = 5.6 +(4*wind_)
        

        else:
            

            h_conv = pow(7.2*(wind_), .78)


        return h_conv


    def compute_solar_shadow(self):

    # Date from future
        forecast_object = self.forecast_variables_


        rail_model = self.rail_model_


        date = self.convert_to_timestamp(forecast_object.dateBegin_)


        n, g, L, l, ep = self.compute_ecliptic_coordinates(date_ = date)


        phi,alpha,L = self.compute_celestial_and_local_coord(n,
                                                            g,
                                                            L,
                                                            l,
                                                            ep,
                                                            forecast_object.lat_,
                                                            forecast_object.lon_)


        projected_points = self.compute_xy_projection(phi, alpha, rail_model.model)

        
        projected_x = projected_points[:,0]


        projected_y = projected_points[:,1]


        sum_of_products = 0


        for idx in range(len(projected_x)-2):


            sum_x_projected = (projected_x[idx] + projected_x[idx+1])


            dif_y_projected = (projected_y[idx] - projected_y[idx+1])


            sum_of_products += (sum_x_projected*dif_y_projected)

        
        s_shadow = (1/2) * sum_of_products


        solar_incidence = s_shadow * np.sin(alpha)


        return (solar_incidence, alpha)


    def set_radiation_balance_eq(self):

        T_r = 50


        fcast_object = self.forecast_variables_


        rail_model = self.rail_model_


        SA = 100 - rail_model.abs_ #rail_model absorptivity


        e_r = rail_model.epsilon_ #rail_model reflectance


        radiation = fcast_object.radiation_ #Solar radiation (W/m²)


        T_air = fcast_object.temperature_ 


        T_sky = fcast_object.T_sky_


        Ar = rail_model.Ar_ * 10e-6 #rail_model Surface area m²


        Vr = rail_model.Vr_  *10e-9 #rail_model Volume m³
        

        Rm = rail_model.Rm_ #rail_model mass


        ro_r = 2*Rm//Vr #rail_model density


        c_r = rail_model.c_r_ #rail_model specific heat


        sigma = 5.670374419e-18 #stephan-boltzmann


        h_conv = self.compute_h_conv(fcast_object.wind_)


        A_s, alpha = self.compute_solar_shadow()


        DT_r = ((SA * radiation  * A_s * np.sin(alpha))
                - ((h_conv * Ar (T_r - T_air))
                + (e_r * sigma * Ar * (pow(T_r,4) - pow(T_sky,4))))
                / (ro_r * c_r * Vr))


        return DT_r

    @staticmethod
    def compute_ode(DT_r_, T_r_):


        solver = scp.integrate.solve_ivp(DT_r_, [0,12], T_r_)
        
        
        return(solver)  



#(E_rad + E_conv + E_reflect) - E_in

# E_reflect = 





        

        











